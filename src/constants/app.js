export const BASE_URL = 'https://www.googleapis.com/youtube/v3/search';
export const API = 'AIzaSyBbBlfM1ilxaDmUqh4y9QMCvjyapR6RjZs';
export const MIN_QUERY_LENGTH = 3;

export const ACTION_TYPES = {
    FETCH_VIDEOS_START: 'FETCH_VIDEOS_START',
    FETCH_VIDEOS_SUCCESS: 'FETCH_VIDEOS_SUCCESS',
    FETCH_VIDEOS_FAILURE: 'FETCH_VIDEOS_FAILURE',
    SET_ACTIVE_VIDEO: 'SET_ACTIVE_VIDEO',
};
